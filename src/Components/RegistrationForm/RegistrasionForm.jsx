import React, { useState } from "react";
import { useForm } from "react-hook-form";

import "./RegistrationForm.scss";

function RegistrasionForm() {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    getValues,
  } = useForm();

  const [passwordSame, setPasswordSame] = useState(false);

  function onSubmit(data, e) {
    e.preventDefault();
    console.log(data);
    const values = getValues();
    if (values.password !== values.confirmPassword) {
      setPasswordSame(true);
    } else {
      reset(alert("Congrats! You are now part of the system."));
      setPasswordSame(false);
    }
  }

  return (
    <div className="registration">
      <form className="registration__form" onSubmit={handleSubmit(onSubmit)}>
        <h2>Registrasion Form</h2>
        <div className="registration__form__container__two">
          <div className="registration__form__item registration__form__container__two__first">
            <label htmlFor="fname">First Name :</label>
            <input
              type="text"
              name="fname"
              id="fname"
              {...register("firstname", {
                required: true,
                minLength: 2,
                maxLength: 20,
              })}
            />
          </div>
          <div className="registration__form__item registration__form__container__two__second">
            <label htmlFor="lname">Last Name :</label>
            <input
              type="text"
              name="lname"
              id="lname"
              {...register("lastname", {
                required: true,
                minLength: 2,
                maxLength: 20,
              })}
            />
          </div>
        </div>
        {errors.firstname?.type === "required" && (
          <h5 className="errormsg">Please enter firstname</h5>
        )}
        {errors.lastname?.type === "required" && (
          <h5 className="errormsg">Please enter lastname</h5>
        )}

        <div className="registration__form__item">
          <label htmlFor="email">Email :</label>
          <input
            type="email"
            name="email"
            id="email"
            {...register("email", { required: true })}
          />
        </div>
        {errors.email && <h5 className="errormsg">Please enter your Email</h5>}

        <div className="registration__form__container__two">
          <div className="registration__form__item registration__form__container__two__first">
            <label htmlFor="pass">Password :</label>
            <input
              type="password"
              name="pass"
              id="pass"
              {...register("password", {
                required: "Please enter password",
                minLength: {
                  value: 3,
                  message: "Password length atlease 3 character",
                },
              })}
            />
          </div>
          <div className="registration__form__item registration__form__container__two__second">
            <label htmlFor="cpass">Confirm Password :</label>
            <input
              type="password"
              name="cpass"
              id="cpass"
              {...register("confirmPassword", { required: true })}
            />
          </div>
        </div>
        {errors.password && (
          <h5 className="errormsg">{errors.password?.message}</h5>
        )}
        {errors.confirmPassword && (
          <h5 className="errormsg">Please enter your Confirm Password</h5>
        )}
        {passwordSame && <h5 className="errormsg">Password does not match</h5>}

        <div className="registration__form__container__two">
          <div className="registration__form__item registration__form__container__two__first">
            <label htmlFor="department">Department :</label>
            <select name="gender" id="gender" {...register("department")}>
              <option value="Reactjs">Reactjs</option>
              <option value="Nodejs">Nodejs</option>
            </select>
          </div>
          <div className="registration__form__item registration__form__container__two__second">
            <label htmlFor="phone">Phone Number :</label>
            <input
              type="number"
              name="phone"
              id="phone"
              {...register("phoneNumber", {
                required: "Please enter your PhoneNumber",
                maxLength: {
                  value: 10,
                  message: "Please enter correct PhoneNumber",
                },
                minLength: {
                  value: 10,
                  message: "Please enter correct PhoneNumber",
                },
              })}
            />
          </div>
        </div>
        {errors.phoneNumber && (
          <h5 className="errormsg">{errors.phoneNumber?.message}</h5>
        )}

        <div className="registration__form__container__two">
          <div className="registration__form__item registration__form__container__two__first">
            <label htmlFor="dob">Date of Birth :</label>
            <input
              type="date"
              name="dob"
              id="dob"
              {...register("dob", { required: true })}
            />
          </div>
          <div className="registration__form__item registration__form__container__two__second">
            <label>Gender :</label>
            <div className="registration__form__item__radio">
              <input
                className="radioinput"
                type="radio"
                id="male"
                name="fav_language"
                value="Male"
                {...register("gender", {
                  required: "Please select your gender",
                })}
              />
              <label htmlFor="male">Male</label>
              <input
                className="radioinput"
                type="radio"
                id="female"
                name="fav_language"
                value="Female"
                {...register("gender", {
                  required: "Please select your gender",
                })}
              />
              <label htmlFor="female">Female</label>
            </div>
          </div>
        </div>
        {errors.dob && <h5 className="errormsg">Please enter date of birth</h5>}
        {errors.gender && (
          <h5 className="errormsg">{errors.gender?.message}</h5>
        )}

        <div className="registration__form__checkbox">
          <input
            type="checkbox"
            id="agree"
            name="agree"
            value="agree"
            {...register("agree", { required: true })}
          />
          <label htmlFor="agree"> Agree on terms and conditions?</label>
        </div>
        {errors.agree && (
          <h5 className="errormsg">Please agree on our terms and conditions</h5>
        )}

        <input type="submit" className="btn" value="Submit" />
      </form>
    </div>
  );
}

export default RegistrasionForm;
