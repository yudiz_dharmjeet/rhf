import React from "react";

import "./App.scss";
import { RegistrasionForm } from "./Components";

function App() {
  return (
    <div className="app">
      <RegistrasionForm />
    </div>
  );
}

export default App;
